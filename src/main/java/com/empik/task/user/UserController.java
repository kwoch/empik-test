package com.empik.task.user;

import lombok.extern.slf4j.Slf4j;

import com.empik.task.user.model.ErrorMessage;
import com.empik.task.user.model.GitDto;
import com.empik.task.user.model.UserResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import org.springframework.web.client.RestTemplate;

import static com.empik.task.user.model.UserResponseDtoFactory.getUserResponseDto;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Slf4j
@RestController
@RequestMapping("users")
public class UserController {

  private static final String GIT_USERS_API = "https://api.github.com/users/";

  @Autowired
  private UserService userService;

  @Autowired
  private RestTemplate restTemplate;

  @GetMapping("/{login}")
  public ResponseEntity<UserResponseDto> getUser(@PathVariable String login) {

    ResponseEntity<GitDto> entity = restTemplate.getForEntity(
        GIT_USERS_API + login,
        GitDto.class);

    GitDto gitUser = entity.getBody();
    String calculations = userService.getCalculationsForUser(
        gitUser.getFollowers(),
        gitUser.getPublicRepos());

    UserResponseDto userResponseDto = getUserResponseDto(gitUser, calculations);
    userService.registerRequest(userResponseDto.getLogin());
    return ResponseEntity.ok(userResponseDto);
  }

  @ExceptionHandler(NotFound.class)
  @ResponseStatus(NOT_FOUND)
  public ErrorMessage handleNotFoundErrors() {
    return ErrorMessage.builder().status(NOT_FOUND).message("User doesn't found").build();
  }

  @ResponseStatus(value = INTERNAL_SERVER_ERROR)
  @ExceptionHandler(value = Exception.class)
  public ErrorMessage handleAllException(Exception e) {
    log.warn("An unknown Exception Occurred: " + e);
    return ErrorMessage
        .builder()
        .status(INTERNAL_SERVER_ERROR)
        .message("Something went wrong. Try again later or contact with administrator")
        .build();
  }
}