package com.empik.task.user.model;

public class UserResponseDtoFactory {

  public static UserResponseDto getUserResponseDto(GitDto gitDto, String calculations) {

    return UserResponseDto
        .builder()
        .id(gitDto.getId().toString())
        .login(gitDto.getLogin())
        .name(gitDto.getName())
        .type(gitDto.getType())
        .avatarUrl(gitDto.getAvatarUrl())
        .createdAt(gitDto.getCreatedAt())
        .calculations(calculations)
        .build();
  }
}
