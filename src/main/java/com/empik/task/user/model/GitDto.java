package com.empik.task.user.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class GitDto {

  private Long id;
  private String login;
  private String name;
  private String type;
  private Long followers;

  @JsonProperty("avatar_url")
  private String avatarUrl;

  @JsonProperty("created_at")
  private String createdAt;

  @JsonProperty("public_repos")
  private Long publicRepos;
}
