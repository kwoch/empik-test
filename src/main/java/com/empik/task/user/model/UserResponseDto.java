package com.empik.task.user.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserResponseDto {

  private String id;
  private String login;
  private String name;
  private String type;
  private String avatarUrl;
  private String createdAt;
  private String calculations;
}