package com.empik.task.user;

import com.empik.task.user.model.UserRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository  extends JpaRepository<UserRequest, Long> {
  UserRequest findByLogin(String login);
}
