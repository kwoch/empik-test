package com.empik.task.user;

import com.empik.task.user.model.UserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_UP;

@Service
class UserService {

  @Autowired
  public UserRepository userRepository;

  void registerRequest(String login) {
    UserRequest userRequest = userRepository.findByLogin(login);
    if (userRequest != null) {
      userRequest.setRequestCount(userRequest.getRequestCount() + 1);
    } else {
      userRequest = new UserRequest();
      userRequest.setLogin(login);
    }

    userRepository.save(userRequest);
  }

  String getCalculationsForUser(Long userFollowers, Long userPublicRepos) {
    if (userFollowers == null || userFollowers == 0 || userPublicRepos == null)
      return null;

    BigDecimal followers = valueOf(userFollowers);
    BigDecimal publicRepos = valueOf(userPublicRepos);
    followers = valueOf(6).divide(followers, 2, HALF_UP);
    publicRepos = valueOf(2).add(publicRepos);
    return followers.multiply(publicRepos).setScale(2, HALF_UP).toString();
  }
}
