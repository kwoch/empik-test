package com.empik.task.user;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.assertThat;

public class UserControllerTest {

  private static final Long FOLLOWERS = 100L;
  private static final Long PUBLIC_REPOS = 10L;
  private static final BigDecimal FINAL_SOLUTION = valueOf(0.72);

  private UserService userService;
  private UserRepository userRepository;

  @BeforeEach
  public void setUp() {
    userService = new UserService();
  }

  @Test
  public void calculationForUserTest() {
    String asd = userService.getCalculationsForUser(FOLLOWERS, PUBLIC_REPOS);
    BigDecimal solution = valueOf(Double.parseDouble(asd));
    assertThat(solution).isEqualTo(FINAL_SOLUTION);
  }
}